﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TutorialForInheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            IPolygon A = new Rectangle(4, 5);
            IPolygon B = new Square(3);
            // This "PropertyWriter" class works based on IPoligon interface.
            PropertyWriter writer = new PropertyWriter();
            writer.WritePerimeterAndAreaOfPolygon(A);
            writer.WritePerimeterAndAreaOfPolygon(B);
            // Rectangle and Square are IPoligon of course.
            Square C = new Square(6);
            writer.WritePerimeterAndAreaOfPolygon(C);

            Rectangle D = new Rectangle(7, 7);
            // Rectange is not Square
            //Square E = D;
            // Square is Rectangle too
            Rectangle F = C;
            // It's reference is Rectangle, but it's "Name" property is from Square
            writer.WritePerimeterAndAreaOfPolygon(F);
            Console.ReadLine();
        }
    }
}
