﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TutorialForInterface
{
    interface IPolygon
    {
        string Name { get; }
        int Area();
        int Perimeter();
    }
}
