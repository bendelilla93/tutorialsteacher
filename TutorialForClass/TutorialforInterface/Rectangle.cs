﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TutorialForInterface
{
    /// <summary>
    /// This is an example class for TutorialsTeacher C# Class section
    /// </summary>
    class Rectangle : IPolygon
    {
        private int a;
        private int b;

       public string Name
        {
            get { return "RECTANGLE"; }
        }
        /// <summary>
        /// This is a public property to provide access to private field.
        /// But we don't allow to set it outside the class 
        /// </summary>
        public int sideA
        {
            get { return a; }
            private set { a = value; }
        }
        /// <summary>
        /// This is a public property to provide access to private field.
        /// But we don't allow to set it outside the class.
        /// </summary>
        public int sideB
        {
            get { return b; }
            private set { b = value; }
        }
        /// <summary>
        /// This is a public method to calculate the area of a Rectangle instance.
        /// </summary>
        public int Area()
        {
            return a * b;
        }
        /// <summary>
        /// This is a public method to calculate the perimeter of a Rectangle instance.
        /// </summary>
        public int Perimeter()
        {
            return HalfPerimeter() + HalfPerimeter();
        }
        /// <summary>
        /// This is a private utility method to calculate the half of the perimeter of a Rectangle instance.
        /// But this cannot be called outside the class. 
        /// </summary>
        private int HalfPerimeter()
        {
            return a + b;
        }
        /// <summary>
        /// This is the constructor which ensures to create instances of Rectangle class with specific parameter.
        /// </summary>
        /// <param name="a">Size of one side of the rectangle </param>
        /// <param name="b">Size of another side of the rectangle </param>
        public Rectangle(int a, int b)
        {
            // Use 'this' keyword for the field of class because the name of the field and parameter are the same. 
            this.a = a;
            this.b = b;
        }
    }
}

